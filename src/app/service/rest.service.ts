import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { saveAs } from 'file-saver';

const endpoint = 'http://localhost:60989/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }


  getUrl() {
    return endpoint;
  }

  async getFile(documentid: number,docname:string) {
    //this.http.get(endpoint + 'Document/download/' + documentid, {
      //responseType: 'arraybuffer'
    //}
    //).subscribe(response => this.downLoadFile(response, "APPLICATION/octet-stream"));
    //return this.http.get(endpoint + 'Document/download/' + documentid).toPromise();
    this.http.get(endpoint + 'Document/download/' + documentid, {responseType: 'blob'}).subscribe(response => {
      try {
          let isFileSaverSupported = !!new Blob;
      } catch (e) { 
          console.log(e);
          return;
      }
      let blob = new Blob([response], { type: 'APPLICATION/octet-stream' });
      saveAs(blob, docname);
    });
  }
  async getDocuments(username: string) {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Access-Control-Allow-Origin','*');
    headers = headers.append('Accept', 'application/json');
    headers = headers.append('Username',username);
    
    return this.http.get(endpoint + 'Document',{
     headers
   }).toPromise();
  }

  downLoadFile(data: any, type: string) {
    let blob = new Blob([data], { type: type });
    let url = window.URL.createObjectURL(blob);
    let pwa = window.open(url);
    if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
      alert('Please disable your Pop-up blocker and try again.');
    }
  }
}
