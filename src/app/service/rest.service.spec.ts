import { TestBed, getTestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RestService } from './rest.service';
import { HttpClient } from '@angular/common/http';

describe('RestService', () => {
  let injector: TestBed;
  let service: RestService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RestService]
    });
  });

  it('should be initialized', inject([RestService], (rest: RestService) => {
    expect(RestService).toBeTruthy();
  }));
  
});
