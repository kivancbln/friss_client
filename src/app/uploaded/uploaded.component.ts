import { Component, OnInit } from '@angular/core';
import { RestService } from '../service/rest.service';

@Component({
  selector: 'app-uploaded',
  templateUrl: './uploaded.component.html',
  styleUrls: ['./uploaded.component.css']
})
export class UploadedComponent implements OnInit {
  documents: Object;
  apiurl: string;

  constructor(public rest: RestService) { }

  ngOnInit() {
    this.getDocuments();
    this.apiurl = this.rest.getUrl();
  }

  async getFile(docid:number,docname:string){
    debugger;
    await this.rest.getFile(docid,docname);

  }

  async getDocuments(){
    let response = await this.rest.getDocuments("kivanc");
    this.documents = response["model"];
  }


  
}


