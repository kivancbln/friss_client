import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadedComponent } from './uploaded/uploaded.component';


const routes: Routes = [
  {
    path: 'uploaded',
    component: UploadedComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
